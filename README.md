# paperless-ngx
Compose file to run [paperless-ngx](https://docs.paperless-ngx.com/) behind Authentik authentication, compatible with Portainer installations.
